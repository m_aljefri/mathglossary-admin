# frozen_string_literal: true

# config valid for current version and patch releases of Capistrano
lock '~> 3.10.1'

set :repo_url, 'git@gitlab.com:m_aljefri/salah.git'
set :rails_env, fetch(:stage)

# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :false

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, '.env'

# Default value for linked_dirs is []
append :linked_dirs, 'node_modules'

# Default value for keep_releases is 5
set :keep_releases, 2

after  'deploy:updated', 'yarn:install'
before 'deploy:publishing', 'yarn:build'

namespace :yarn do
  desc 'Install NPM Packages'
  task :install do
    on roles(:all) do
      within release_path do
        execute 'yarn', 'install'
      end
    end
  end

  desc 'Build for the Production'
  task :build do
    on roles(:all) do
      within release_path do
        execute 'yarn', 'build', fetch(:react_env_variables)
      end
    end
  end
end