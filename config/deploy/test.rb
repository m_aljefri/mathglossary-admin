# frozen_string_literal: true

set :application, 'laan-admin-test'
set :deploy_to, "/var/www/html/#{fetch(:application)}"
set :branch, 'test'

server '202.164.34.20',
       user: 'clicksandbox',
       roles: %w[web],
       ssh_options: {
         user: 'clicksandbox', # overrides user setting above
         keys: %w[~/.ssh/id_rsa],
         forward_agent: false,
         auth_methods: %w[publickey]
         #  password: ''
       }
