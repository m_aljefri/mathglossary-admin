import Axios from 'axios';
import { I18n } from 'react-redux-i18n';

const headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'locale': 'ar',
  'access': 'admin',
};

const http = Axios.create({
  baseURL: process.env.REACT_APP_BASE_API,
  headers,
});

// Axios.defaults.headers = headers();
http.interceptors.response.use(null, ({ response }) => {
  let error = {msg: '', code: null};
  if (response) error = {msg: response.data.error, code: response.status};
  else {
    if (window.navigator.onLine) error.msg = 'Application Error';
    else error.msg = I18n.t('No Internet Connection');
  }
  return Promise.reject(error);
});

export default http;