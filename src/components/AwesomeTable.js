import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'components/Buttons';
import _ from 'lodash';

const AwesomeTable = ({
  headers, next_page, data, handleNextPage, footer, className, ...props
}) => (
  <React.Fragment>
    {console.log({next_page})}
    <table {...props} className={className}>
      <thead>
        <tr>
          {headers.map((head, key) => (
            <th key={key} {...head.props}>
              {head.title ? head.title : head.name}
            </th>
          ))}
        </tr>
      </thead>
      <tbody className="scroll-y">
        {_.isEmpty(data) ? (
          <tr>
            <td colSpan={headers.length} className="no-data">
              No data
            </td>
          </tr>
        ) : data.map((item, key) => (
          <tr key={key}>
            {headers.map((config, index) => {
              if (config.render) {
                return <td {...config.tdProps} key={index}>{config.render(item)}</td>;
              }
              return (
                <td {...config.tdProps} key={index}>
                  {handleNoData(getDataByKey(item, config.key))}
                </td>);
            })}
          </tr>
        ))}
        {footer && <tr className="footer">{footer}</tr>}
      </tbody>
      <footer>
      </footer>
    </table>
    <div style={{ display: 'flex',}}>
      {next_page && <Button fixedSize onClick={() => handleNextPage({page: next_page})}>التالي</Button>}
      {next_page && next_page > 2 && <Button fixedSize onClick={() => handleNextPage({page: (next_page - 2) })}>السابق</Button>}
    </div>
  </React.Fragment>
);

AwesomeTable.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.shape({
    i18n: PropTypes.string,
    render: PropTypes.func,
    width: PropTypes.number,
    key: PropTypes.string,
    props: PropTypes.object,
  }).isRequired).isRequired,

  footer: PropTypes.node,
  data: PropTypes.arrayOf(Object).isRequired,

  className: PropTypes.string,
};


export const getDataByKey = (object, key = null) => {
  if (!key) return object;
  const keys = key.split('.');
  if (keys.length === 0) return object;
  let result = object;
  keys.map((item) => {
    if (result) {
      result = result[item];
    }
    return null;
  });
  return result;
};

export function handleNoData(data) {
  const output = data === '' || data == null ? '-' : data;
  return output;
}


export default AwesomeTable;
