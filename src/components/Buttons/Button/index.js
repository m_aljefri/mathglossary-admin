import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  border-radius: ${({ theme }) => theme.sizes.radius};
  border: none;
  text-decoration: none;
  background-color: ${props => props.brand ?
    props.theme.colors.brand :
    props.theme.colors.primary};
  color: #fff;
  opacity: .8;
  text-align: center;
  padding: 5px 1rem;
  height: 2.7em;
  margin: 0 2px 15px;

  display: block;
  max-width: ${({ fixedSize }) => fixedSize && '10rem'};
  width: 100%;

  &:disabled {
    opacity: 0.3 !important;
  }

  &:hover {
    opacity: 1;
    color: #fff;
  }
`

export const Button = ({ fetching, disabled, children, ...props })  => (
  <StyledButton {...props} disabled={fetching || disabled} >
    {fetching ? '...' : children}
  </StyledButton>
)
