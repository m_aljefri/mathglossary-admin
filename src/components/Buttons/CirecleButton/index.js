import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ButtonWrapper } from './styled';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { COLORS } from '../../constants';


export const CirecleButton = ({ icon, to, color, background, ...props }) => {
  const WrapLink = (props) => !!(to) ? <Link {...props} /> : <span {...props} />;
  return (
    <WrapLink to={to}>
      <ButtonWrapper color={color} background={background}  {...props}>
        <FontAwesomeIcon icon={icon} />
      </ButtonWrapper>
    </WrapLink>
  );
};

CirecleButton.propTypes = {
  color: PropTypes.oneOf(COLORS),
  background: PropTypes.oneOf(COLORS),
};