import React from 'react';
import Calander from './index';
import { ErrorMessage } from 'components/Massage';

const CalanderField = ({
  input: {
    onChange,
    value,
    ...input,
  },
  meta: {
    invalid,
    error,
    touched,
  },
  options,
  valueKey,
  nameKey,
  ...props
}) => (
  <div>
    <Calander
      {...props}
      {...input}
      value={value ? new Date(value) : new Date()}
      onChange={value => {
        onChange(value)
      }}
      format="YYYY-MM-DD"
    />
    {touched && invalid && error && <ErrorMessage>{error}</ErrorMessage>}
  </div>
);

export default CalanderField;
