import { DateTimePicker } from 'react-widgets'
import Field from './Field';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
import 'react-widgets/dist/css/react-widgets.css';


Moment.locale('en');
momentLocalizer();
DateTimePicker.Field = Field;

export default DateTimePicker;
