import React from 'react';
import Dropzone from 'react-dropzone';
import { Button } from 'components/Buttons';

export default class extends React.Component {
  state = { files: [] };

  renderPreview = (files) => {
    const filesWithPreview = files.map(file => Object.assign(file, {
      preview: URL.createObjectURL(file)
    }))
    return (
      filesWithPreview.map((file, i) =>
        <li key={i}>
          <img key={i}
              src={file.preview} alt="preview"/>
          <p>{file.name}</p>
        </li>,
      )
    );
  };

  onDrop = (files) => {
    this.props.input.onChange(files);
    this.setState({
      files: files.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      }))
    });
  }

  render () {
    const field = this.props;
    const { files } = this.state;
    let dropzoneRef;
    const thumbs = files.map(file => (
      <div key={file.name}>
        <div>
          <img
            src={file.preview}
            alt="preview"
          />
        </div>
      </div>
    ));
    return (
     <React.Fragment>
     <Dropzone
         style={{
           width: '200px',
           height: '200px',
           borderWidth: '2px',
           borderColor: 'rgb(102, 102, 102)',
           borderStyle: 'dashed',
           borderRadius: '5px',
           padding: '20px',
         }}
         ref={(node) => { dropzoneRef = node }}
         name={field.name}
         onClick={() => {
           console.log('click on zone')
         }}
         onDrop={this.onDrop}
         maxSize={5242880}
         multiple={false}
         accept={'image/*'}
         className="drop-zone"
     >
       {({getRootProps, getInputProps, isDragActive}) => {
            return (
              <div
                {...getRootProps()}
              >
                <input {...getInputProps()} />
                {
                  isDragActive ?
                    <p>Drop files here...</p> :
                    <p>Try dropping some files here, or click to select files to upload.</p>
                }
              </div>
            )
          }}
     </Dropzone>
     {field.meta.touched &&
       field.meta.error &&
       <span className="error">{field.meta.error}</span>}
       {files && Array.isArray(files) && (
          <ul>
            {thumbs}
          </ul>
        )}
        <Button
          type="button"
          style={{margin: '5px'}}
          onClick={() => {
            console.log('button', dropzoneRef)
            dropzoneRef.open();
          }}
        >
          إضافة صورة
        </Button>
     </React.Fragment>      
   )
  }
}