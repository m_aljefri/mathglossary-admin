import React from 'react';
import styled from 'styled-components';
import { Image } from './Image';

const StyledInpute = styled.input`
  opacity: 0;
  height: 150px;
  position: absolute;
  top:0;
  &:hover {
    background: #ccc;
    opacity: .5;
  }
`

export class ImageField extends React.Component {
  state = {
    file: {},
    files: [], // may we use it on future
  }
  handleChange = (event) => {
    // get the files
    const { files } = event.target;

    // Process each file
    const allFiles = [];
    for (let i = 0; i < files.length; i += 1) {
      const file = files[i];

      // Make new FileReader
      const reader = new FileReader();

      // Convert the file to base64 text
      reader.readAsDataURL(file);

      // on reader load somthing...
      reader.onload = () => {
        // Make a fileInfo Object
        const fileInfo = {
          file_file_name: file.name,
          type: file.type,
          size: `${Math.round(file.size / 1000)} kB`,
          file_size: Math.round(file.size / 1000),
          file: reader.result,
        };

        // Push it to the state
        allFiles.push(fileInfo);

        // If all files have been proceed
        if (allFiles.length === files.length) {
          
          this.setState({ file: allFiles[0] });
        }
      }; // reader.onload
    } // for
  }

  render() {
    const { value, name, ...props } = this.props;
    const { file } = this.state;
    const imageSource = file && file.file ? file.file : value;
    return (
      <div style={{
        position: 'relative', maxWidth: 150, overflow: 'hidden', borderRadius: 75, }}>
        <StyledInpute onChange={this.handleChange} {...props} type="file" name={name} />
        <Image src={imageSource} />
      </div>
    );
  }
};
