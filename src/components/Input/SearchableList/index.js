import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ListItem from './ListItem';

class SearchableDropdownList extends Component {
  state = {
    user_name: '',
    show: false,
  };

  componentWillUnmount = () => {
    clearTimeout(this.methodTimer);
  };

  methodTimer = null; // set the timer id

  showList = boolean => () => {
    setTimeout(() => this.setState({ show: boolean }), 300); // delay to enable selection
  };

  handleTyping = (event) => {
    const { value } = event.target;
    this.props.input.onChange(value);
    this.setState({ user_name: value });

    clearTimeout(this.methodTimer); // clear timer once typing again
    this.methodTimer = setTimeout(() => this.handleSearching(value), 2000);
  };

  handleSearching = (search) => {
    const params = { search, limit: 5 }; // Backend should enable limit in this api.
    this.props.getAllEmployeesLoad(params);
  };

  handleSelect = item => () => {
    this.props.input.onChange(item.id);
    this.setState({ user_name: item.name });
  };

  render() {
    const { show, fetching, user_name } = this.state;
    const {
      meta: { touched, error },
      className,
      employee_all,
    } = this.props;
    return (
      <div className="user-droplist">
        <div className={classNames('awesome-select', className)}>
          <input
            value={user_name}
            onFocus={this.showList(true)}
            onBlur={this.showList(false)}
            onChange={this.handleTyping}
          />
          <input type="hidden" {...this.props} {...this.props.input} />
          {touched &&
            error && (
              <small className="form-text text-muted color-red">Please select an Employee</small>
            )}
          <ListItem
            fetching={fetching}
            isOpen={show}
            data={employee_all}
            onSelect={this.handleSelect}
          />
        </div>
      </div>
    );
  }
}

SearchableDropdownList.propTypes = {
  getAllEmployeesLoad: PropTypes.func.isRequired,

  employee_all: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  valueField: PropTypes.string,
  textField: PropTypes.string,
  imageField: PropTypes.string,
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
  }),
  meta: PropTypes.shape({
    error: PropTypes.string,
    invalid: PropTypes.bool.isRequired,
  }).isRequired,
  className: PropTypes.string,
};


export default SearchableDropdownList;
