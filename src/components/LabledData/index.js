import React from 'react';

const LabledData = ({
  lable,
  data,
}) => (
  <div>
    <strong>{lable}</strong>
    <div>{data}</div>
  </div>
)

export default LabledData;
