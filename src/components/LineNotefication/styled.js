import styled from 'styled-components';

const background = ({ color = 'success', theme }) => {
  switch (color) {
    case 'success':
      return theme.colors[color];
  
    default:
      return '#ddd';
  }
}

export const LinerWrapper = styled.div`
  position: sticky;
  top: 0;
  right: 0;
  left: 0;
  background: ${background};
  color: #fff;
  text-align: center;
`;
