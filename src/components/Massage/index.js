import styled from 'styled-components';

export const ErrorMessage = styled.small`
  color: ${({ theme, color }) => color || theme.colors.danger};
`;

export const ErrorMessageContainer = styled.div`
  padding: 20px;
  background: rgba(${({ theme }) => theme.colors.danger}, .03);
  border: 1px solid rgb(${({ theme, color }) =>  color || theme.colors.danger});
  margin-bottom: 25px;
  border-radius: 10px;
`;
