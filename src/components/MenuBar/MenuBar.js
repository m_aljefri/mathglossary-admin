import React from 'react';
import Wrapper from 'components/Wrapper';
import { NavLink } from "react-router-dom";
import { MenuBar, SigninBtn } from './styled';


export default ({
  toggleSigninModal,
  toggleSignupModal,
  toggleModal,
  logout,
  token,
}) => {
  const toggleSignin = () => { toggleSigninModal(); toggleModal(); }
  const toggleSignup = () => { toggleSignupModal(); toggleModal(); }
  const publicButtons = [
    { childrens: 'تسجيل الدخول', onClick: toggleSignin, primary: false },
    { childrens: 'إنشاء حساب', onClick: toggleSignup, primary: true },
  ]

  const privateButtons = [
    { childrens: 'تسجيل الخروج', onClick: logout, primary: false },
  ];

  const buttons = token ? privateButtons : publicButtons;
  return (
    <MenuBar>
      <Wrapper flex between style={{ flexDirection: 'row-reverse' }}>
        <div>
          {buttons.map(({ childrens, onClick, ...rest }) => (
            <SigninBtn key={childrens} onClick={onClick} {...rest}>{childrens}</SigninBtn>
          ))}
        </div>
        <nav>
          <NavLink to="/">الرئيسية</NavLink>
        </nav>
      </Wrapper>
    </MenuBar>
  );
};
