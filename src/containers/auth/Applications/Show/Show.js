import React from 'react'
import LabledData from 'components/LabledData';
import FlexBox from 'components/FlexBox';
import { modelName } from '../config';

class Show extends React.Component {
  componentDidMount() {
    this.props.getRecord(this.props.match.params.id)
  }
  
  state = { isOpen: false }
  handleLoan = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }

  header = [
    { title: 'المبلغ النقدي', key: 'cash_amount' },
    { title: 'مبلغ الشبكة', key: 'network_amount' },
    { title: 'مجموع الصندوق', key: 'total_fund' },
    { title: 'المجموع الكلي', key: 'total_summation' },
    { title: 'المتبقي', key: 'amount_difference' },
    { title: 'التاريخ', key: 'date' },
    { title: 'الملاحظة', key: 'notes' },
  ];
  render() {
    const { details } = this.props[modelName];
    return (
      <div>
        <div>
          <h2>معلومات المبيعات</h2>
          <FlexBox>
            {this.header.map(item => (
              item.key && <LabledData
                lable={item.title}
                data={details[item.key]}
              />
            ))}
          </FlexBox>
        </div>
      </div>
    )
  }
}

Show.propTypes = {

}

export default Show;