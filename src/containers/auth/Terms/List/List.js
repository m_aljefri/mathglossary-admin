import React from 'react';
// import PropTypes from 'prop-types';
import { LinkButton } from 'components/Buttons';
import queryString from 'qs'
import { modelName } from '../config';
import { CirecleButton } from 'components/Buttons';
import AwesomeTable from 'components/AwesomeTable';
import TableWrapper from 'components/TableWrapper';
import { faPencilAlt, faTimes, faSpinner, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class List extends React.Component {
  state = {
    openModal: false,
  }
  componentDidMount () {
    this.props.getRecords();
  };

  componentDidUpdate(prevProps) {
    const prevSearch = queryString.parse(prevProps.location.search)['?search']
    const currentSearch = queryString.parse(this.props.location.search)['?search']
    if (prevSearch && prevSearch !== currentSearch) {
      this.props.getRecords({ search: currentSearch });
    }
  }

  toggleModal = () => {
    this.setState({ openModal: !this.state.openModal });
  }

  handleDelete = (id) => {
    this.props.deleteRecord(id)
  }

  headers = [
    {
      title: 'المصطلح بالعربي',
      key: 'term_ar',
    },
    {
      title: 'المصطلح بلأنجليزي',
      key: 'term_en',
    },
    {
      title: 'شرح المصطلح بلعربي',
      key: 'term_description_ar',
    },
    {
      title: 'شرح المصطلح بلأنحليزي',
      key: 'term_description_en',
    },
    {
      title: 'العمليات',
      render: (record) => (
        <React.Fragment>
          <CirecleButton
            icon={faTimes}
            background="ganger"
            onClick={() => this.handleDelete(record.id)}
          />
          <CirecleButton
            icon={faPencilAlt}
            background="warn"
            color="light"
            onClick={() => this.props.initlizeRecord(record)}
            to={`/${modelName}/${record.id}/edit`}
          />
        </React.Fragment>
      ),
    },
  ]

  render() {
    const { fetching, list, pagination } = this.props[modelName];
    console.log({pagination})
    return (
      <div>
        <LinkButton to={`/${modelName}/new`} >
          <FontAwesomeIcon icon={faPlus} />
        </LinkButton>
        {fetching && <FontAwesomeIcon icon={faSpinner} pulse />}
        <TableWrapper>
          <AwesomeTable next_page={pagination.next_page} handleNextPage={this.props.getRecords} headers={this.headers} data={list} />
        </TableWrapper>
      </div>
    )
  }
};

List.propTypes = {

}

export default List;
