import React from 'react';
// import PropTypes from 'prop-types';
import { LinkButton } from 'components/Buttons';
import styled from 'styled-components';
import queryString from 'qs'
import { modelName } from '../config';
import { CirecleButton } from 'components/Buttons';
import AwesomeTable from 'components/AwesomeTable';
import TableWrapper from 'components/TableWrapper';
import { faPencilAlt, faTimes, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const StyledLink = styled.a`
  text-decoration: dotted;
  color: ${({ theme }) => theme.colors.primary};
`;

// name_ar
// name_en
// url_ar
// url_en
// description_ar
// description_en

class List extends React.Component {
  state = {
    openModal: false,
  }
  componentDidMount () {
    this.props.getRecords();
  };

  componentDidUpdate(prevProps) {
    const prevSearch = queryString.parse(prevProps.location.search)['?search']
    const currentSearch = queryString.parse(this.props.location.search)['?search']
    if (prevSearch && prevSearch !== currentSearch) {
      this.props.getRecords({ search: currentSearch });
    }
  }

  toggleModal = () => {
    this.setState({ openModal: !this.state.openModal });
  }

  handleDelete = (id) => {
    this.props.deleteRecord(id)
  }

  headers = [
    {
      title: 'الموقع بلعربي',
      render: ({ url_ar, name_ar }) => <StyledLink target="_blank" href={url_ar}>{name_ar}</StyledLink>
    },
    {
      title: 'الموقع بلأنحليزي',
      render: ({ url_en, name_en }) => <StyledLink target="_blank" href={url_en}>{name_en}</StyledLink>
    },
    {
      title: 'شرح بلعربي',
      key: 'description_ar',
    },
    {
      title: 'شرح بلأنحليزي',
      key: 'description_en',
    },
    {
      title: 'العمليات',
      render: (record) => (
        <React.Fragment>
          <CirecleButton
            icon={faTimes}
            background="ganger"
            onClick={() => this.handleDelete(record.id)}
          />
          <CirecleButton
            icon={faPencilAlt}
            background="warn"
            color="light"
            onClick={() => this.props.initlizeRecord(record)}
            to={`/${modelName}/${record.id}/edit`}
          />
        </React.Fragment>
      ),
    },
  ]

  render() {
    const { fetching, list } = this.props[modelName];
    return (
      <div>
        <LinkButton to={`/${modelName}/new`} >
          إضافة
        </LinkButton>
        {fetching && <FontAwesomeIcon icon={faSpinner} pulse />}
        <TableWrapper>
          <AwesomeTable headers={this.headers} data={list} />
        </TableWrapper>
      </div>
    )
  }
};

List.propTypes = {

}

export default List;
