import React from 'react';
import { Field } from 'redux-form';
import { modelName } from '../../config';

import LabledData from 'components/LabledData';
import formHoc from 'hocs/formHoc';
import Input from 'components/Input';
import FlexBox from 'components/FlexBox';
import { Button } from 'components/Buttons/Button';
import {
  isRequired,
  isUrl,
} from 'components/redux-form-validations';

// name_ar
// name_en
// url_ar
// url_en
// description_ar
// description_en

const textApiConfig = [
  {
    title: 'الموقع بلعربي',
    validate: [isRequired],
    key: 'name_ar',
  },
  {
    title: 'الموقع بلأنحليزي',
    validate: [isRequired],
    key: 'name_en',
  },
  {
    title: 'شرح بلعربي',
    validate: [isRequired],
    key: 'description_ar',
  },
  {
    title: 'شرح بلأنحليزي',
    validate: [isRequired],
    key: 'description_en',
  },
  {
    title: 'الرابط بلعربي',
    validate: [isRequired, isUrl],
    key: 'url_ar',
  },
  {
    title: 'الرابط بلأنحليزي',
    validate: [isRequired, isUrl],
    key: 'url_en',
  },
];

const Form = ({ handleSubmit, submitting, ...props }) => (
  <form {...props} onSubmit={handleSubmit}>
    <FlexBox>
      {textApiConfig.map(({ title, key, validate, ...props }) => <LabledData
        lable={title}
        data={
          <Field
            {...props}
            name={key}
            placeholder={`اكتب ${title}...`}
            component={Input.Field}
            validate={validate}
          />
        }
      />)}
    </FlexBox>
    <Button fixedSize disabled={submitting} onClick={handleSubmit}>
      تاكيد
    </Button>
  </form>
);

export default formHoc({ form: `${modelName}-from` })(Form);
