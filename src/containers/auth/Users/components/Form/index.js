import React from 'react';
import { Field } from 'redux-form';
import { modelName } from '../../config';

import LabledData from 'components/LabledData';
import formHoc from 'hocs/formHoc';
import Input from 'components/Input';
import FlexBox from 'components/FlexBox';
import { Button } from 'components/Buttons/Button';
import {
  isRequired,
  isEmail,
} from 'components/redux-form-validations';

const textApiConfig = [
  {
    title: 'الاسم',
    validate: [isRequired],
    key: 'name',
  },
  {
    title: 'الايميل',
    validate: [isRequired, isEmail],
    key: 'email',
  },
];

const Form = ({ handleSubmit, submitting, ...props }) => (
  <form {...props} onSubmit={handleSubmit}>
    <FlexBox>
      {textApiConfig.map(({ title, key, validate, ...props }) => <LabledData
        lable={title}
        data={
          <Field
            {...props}
            name={key}
            placeholder={`اكتب ${title}...`}
            component={Input.Field}
            validate={validate}
          />
        }
      />)}
    </FlexBox>
    <Button fixedSize disabled={submitting} onClick={handleSubmit}>
      تاكيد
    </Button>
  </form>
);

export default formHoc({ form: `${modelName}-from` })(Form);
