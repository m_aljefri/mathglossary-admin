import React from 'react'


const asyncHoc = impoertComponent => 
  class componentName extends React.Component {
    state = {
      component: null
    }
    async componentDidMount() {
      const { default: component } = await impoertComponent();
      this.setState({ component })
    };

    render() {
      const { component: Component } = this.state;
      return (
        Component ? 
        <Component /> :
        null
      )
    }
  }

  export default asyncHoc;
