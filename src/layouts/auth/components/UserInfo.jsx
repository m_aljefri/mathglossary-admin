import React from 'react'
import logo from 'assets/img/logo.svg'
export default () => (
  <div className="profile-info">
    <div className="pop-up">
      <div>
        <div>text here</div>
        <div>text here</div>
        <div>text here</div>
      </div>
    </div>
    <div className="img-container">
      <img src={logo} alt="user-info" />
    </div>
  </div>
)
