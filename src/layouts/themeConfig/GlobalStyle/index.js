import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Cairo:400,600,700');

  * {
    margin: 0;
    padding: 0;
    transition: all 200ms
  }
  body {
    margin: 0;
    padding: 0;
    font-family: 'Cairo', sans-serif;
  }

  form {
    display: block;
  }

  a {
    text-decoration: none;
    height: 40;
    color: ${({ theme }) => theme.colors.dark};
  }

  .widget-container.body-font {
    &> div {
    }
  }
`;
