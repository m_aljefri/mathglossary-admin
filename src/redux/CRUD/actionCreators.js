import Api from 'Api';
import pluralize from 'pluralize';

export default function crudActionsCreator(MODEL) {
  const plural = pluralize(MODEL);
  const singular = pluralize.singular(MODEL);
  
  const getRecords = (params = {}, data = null) => {
    const url = plural;
    const payload = Api.get(url, {params}, data);
    return {
      type: `GET_${plural.toUpperCase()}`,
      payload,
    }
  };

  const getRecord = (id, params = {}, data = null) => {
    const url = `${plural}/${id}`
    const payload = Api.get(url, {params}, data);
    return {
      type: `GET_${singular.toUpperCase()}`,
      payload,
    }
  };

  const getOptions = (params = {}) => {
    const url = `${plural}/options`
    const payload = Api.get(url, {params});
    return {
      type: `FETCH_${singular.toUpperCase()}_OPTIONS`,
      payload,
    }
  };
  
  const createRecord = (data = {}, params = {}) => (dispatch) => {
    const url = plural;
    const payload = Api.post(url , data);
    dispatch({
      type: `CREATE_${singular.toUpperCase()}`,
      payload,
    })
    payload.then(() => dispatch(getRecords(params)));
    return payload;    
  };

  const updateRecord = (data = {}, params = {}, id) => (dispatch) => {
    const urlId = data.id || id
    const url = `${plural}/${urlId}`
    const payload = Api.put(url, data);
    dispatch({
      type: `UPDATE_${singular.toUpperCase()}`,
      payload,
    })
    payload.then(() => dispatch(getRecords(params)));
    return payload;  
  };

  const deleteRecord = (id, params= {}) => (dispatch) => {
    const url = `${plural}/${id}`
    const payload = Api.delete(url);
    dispatch({
      type: `DELETE_${singular.toUpperCase()}`,
      payload,
    })
    payload.then(() => dispatch(getRecords(params)));
    return payload;
  }

  const initlizeRecord = (payload) => ({
    type: `INITLIZE_${singular.toUpperCase()}`,
    payload,
  });
  

  return {
    getRecord,
    getOptions,
    getRecords,
    createRecord,
    deleteRecord,
    updateRecord,
    initlizeRecord,
  }
}
