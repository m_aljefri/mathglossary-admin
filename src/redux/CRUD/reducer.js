import pluralize from 'pluralize'

const initialState = {
  message: null,
  list: [],
  details: {},
  fetching: false,
  pagination: {},
  options: {},
};

const fulfilledState = {
  fetching: false,
  message: null,
}

const rejectedState = payload => ({
  fetching: false,
  message: errorHandler(payload),
});

function errorHandler(payload) {
  if (!payload) return 'No Internet connection';
  return payload.data.error; 
}

export default MODEL => (state = initialState, action) => {
  MODEL = MODEL.toUpperCase();
  const { type, payload } = action;
  const singular = pluralize.singular(MODEL);
  const plural = pluralize(MODEL);

  switch (type) {
    case `INITLIZE_${singular.toUpperCase()}`:
      return {
        ...state,
        details: payload,
      };

    case `GET_${plural}_PENDING`:
    case `GET_${singular}_PENDING`:
    case `CREATE_${plural}_PENDING`:
    case `CREATE_${singular}_PENDING`:
      return { ...state, message: null, fetching: true, error: null };

    case `GET_${plural}_FULFILLED`:
      return {
        ...state,
        ...fulfilledState,
        message: payload.data.message,
        list: payload.data[plural.toLowerCase()],
        pagination: payload.data.pagination,
      };

    case `FETCH_${singular.toUpperCase()}_OPTIONS_FULFILLED`:
      return {
        ...state,
        options: payload.data,
      };

    case `CREATE_${singular}_FULFILLED`:
      return {
        ...state,
        ...fulfilledState,
        message: payload.data.message,
        list: [ ...state.list, payload.data[singular.toLowerCase()]]
      };
    case `UPDATE_${singular}_FULFILLED`:
      return {
        ...state,
        ...fulfilledState,
        message: payload.data.message,
        list: state.list.map((item) => {
          if (item.id === payload.data[singular.toLowerCase()].id) {
            return payload.data[singular.toLowerCase()];
          }
          return item;
        })
      };
    case `GET_${singular}_FULFILLED`:
      return {
        ...state,
        ...fulfilledState,
        message: payload.data.message,
        details: payload.data[singular.toLowerCase()], 
      };

    case `GET_${plural}_REJECTED`:
    case `GET_${singular}_REJECTED`:
    case `UPDATE_${singular}_REJECTED`:
    case `CREATE_${singular}_REJECTED`:
      return { ...state, ...rejectedState(payload.response) }; 
 
    default:
      return state;
  }
}
