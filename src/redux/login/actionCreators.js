import Api from 'Api';
import { toggleModal } from 'redux/modal/actionCreatores';

const action = (url, type, data) => dispatch => {
  const payload = Api.post(url, data);
  dispatch({
    type,
    payload,
  });
  return payload.then(() => dispatch(toggleModal()))
};

export const signin = data => action('sessions/signin', 'SIGNIN', data);
