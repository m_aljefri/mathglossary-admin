const initialState = {
  token: '',
  role: false,
  fetching: false,
}

export default function loginReducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case 'SIGNIN_PENDING':
    case 'SIGNUP_PENDING':
      return {
        ...state,
        fetching: true,
      };

    case 'SIGNIN_FULFILLED':
    case 'SIGNUP_FULFILLED':
      return {
        ...state,
        ...payload.data.data,
        token: payload.data.token,
        fetching: false,
      };

    case 'SIGNIN_REJECTED':
    case 'SIGNUP_REJECTED':
      return {
        ...state,
        fetching: false,
        error: 'error'
      };

    case 'LOGOUT':
      return {
        ...state,
        token: false,
      }

    default:
      return state;
  }
  
}