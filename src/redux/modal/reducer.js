const intialState = {
  openModal: false,
  isSigninModal: false,
}

export default (state = intialState, { type, payload }) => {
  switch (type) {
    case 'TOGGLE_MODAL':
      return { ...state, openModal: !state.openModal };
    case 'TOGGLE_SIGN_IN_UP_FORM':
      return { ...state, isSigninModal: payload };
  
    default:
      return state;
  }
}