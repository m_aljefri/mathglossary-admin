import thunkMiddleware from 'redux-thunk';
import promise from 'redux-promise-middleware';
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import { createStore, applyMiddleware, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'

import rootReducer from './combainReducers'
import { toast } from 'react-toastify';
import { I18n } from 'react-redux-i18n';

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const handleDeleteSuccess = () => toast.success(I18n.t('record_was_deleted_successfully'));

const customMiddleWare = store => next => action => {
  const { type, payload } = action;
  const rejectPrefix = '_REJECTED';
  const fulfilledPrefix = '_FULFILLED';
  const deleteSufix= 'DELETE_';
  if (type) {
    if (type.includes(rejectPrefix)) {
      if (!navigator.onLine) return false;
      const response_status = parseInt(payload.code);
      const UnAuthenticatedErrors = [403, 401];
      if (UnAuthenticatedErrors.includes(response_status)) {
        store.dispatch({ type: 'LOGOUT' })
      }
    } else if (type.includes(deleteSufix) && type.includes(fulfilledPrefix)) {
      handleDeleteSuccess();
    }
  }
  next(action);
}

const middleare = applyMiddleware(promise(), thunkMiddleware, customMiddleWare);
const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['auth'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer)

export default function configureStore() {
  const store = createStore(
    persistedReducer,
    composeEnhancers(middleare)
  );
  const persistor = persistStore(store)
  return { store, persistor };
}