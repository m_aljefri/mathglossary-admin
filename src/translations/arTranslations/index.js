const validate = {
  required: 'مطلوب',
}

export default {
  update: 'تعديل',
  new: 'إضافة',
  edit: 'تعديل',
  events: 'الاحداث',
  create: 'إنشأ',
  Actions: 'العمليات',
  feedbacks: 'الاقتراحات',
  mathwords: 'المصطلحات',
  applications: 'التطبيقات العملية',
  testsites: "مواقع الاختبارات",
  suggestion_links: 'الروابط المقترحة',
  teachers: 'المعلمين',
  users: 'المستخدمين',
  email: 'البريد الالكتروني|',
  mobile: 'الجوال',
  confirm_email: 'تاكيد البريد الالكتروني',
  user_cofirmed: 'تم تاكيد البريد الالكتروني بنجاح',
  no_user_has_this_token: 'لا يوجد مستخدم لهذا الرمز',
  click_me: 'اضغط هنا',
  password: 'كلمة المرور',
  validate: { validate },
}
